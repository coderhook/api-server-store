FROM node:12.0.0

WORKDIR /usr/src/server-api

COPY ./ ./

RUN npm install

CMD ["/bin/bash"]