import { Response, Request } from "express"

function setup() {
  const req = {
    body: {},
  } as Request
  const res = {} as any
  const next = jest.fn()
  
  Object.assign(res, {
    status: jest.fn(
      function status(code) {
        this.status = code
        return this
      }.bind(res),
    ),
    json: jest.fn(
      function json(data) {
        this.json = data
        return this
      }.bind(res),
    ),
    send: jest.fn(
      function send(data) {
        this.send = data
        return this
      }.bind(res),
    ),
  })
  return {req, res, next}
}

export default {
  setup
}