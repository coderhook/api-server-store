import User from '../../src/models/User'
import mongoose from 'mongoose'

export const userTest = {
  name: 'test',
  email: 'test@email.com',
  password: 'passwordvalid'
} 

export const newUser = {
  name: 'new user',
  email: 'newuser@email.com',
  password: 'newuserpass'
} 

afterAll( async () =>  {
  await mongoose.connection.close()
})

export const initDataBase = async () => {
  await mongoose.connect(process.env.DB_TEST_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  await User.deleteMany()
  const userTestRegistered = await User.create(userTest)
  return {
    userTestRegistered
  } 
}

export const dbClose = async () => await mongoose.connection.close()
