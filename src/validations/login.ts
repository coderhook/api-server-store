import {LoginSchema} from './schema'

export default async (req, res, next) => {
  const {email, password} = req.body

  try {
    await LoginSchema.validate({ email, password })
    return next()
  } catch(error) {
    return res.status(422).json({
      message: 'Validation failed.',
      data: {
        errors: {
          [error.path]: error.message
        }
      }
    })
  }
}