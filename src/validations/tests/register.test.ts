import utils from '../../../tests-utils/utils'
import registerValidator from '../register'
import User from '../../models/User'
import {initDataBase, userTest, newUser, dbClose} from '../../../tests-utils/fixtures/db'

let result

beforeAll( async () => {
  result = await initDataBase()
})

afterAll( async () =>  {
  dbClose()
})

const validData = newUser

describe('Register Validation', () => {
  test('should call next when user data is valid', async () => {
    const { req, res, next } = utils.setup()
    req.body = validData

    await registerValidator(req, res, next)

    expect(next).toHaveBeenCalled()
  })

  test('should throw validation error if user already exists', async () => {
    const { req, res, next } = utils.setup()
    req.body = validData
    const registerUser = await User.create(validData)

    const result = await registerValidator(req, res, next)

    expect(next).not.toHaveBeenCalled()
    expect(result.status).toBe(422)
    expect(result.json.message).toBe('Validation failed.')
    
  })
})