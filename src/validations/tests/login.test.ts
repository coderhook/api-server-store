import utils from '../../../tests-utils/utils'
import loginValidator from '../login'

const validData = {
  email: 'test@email.com',
  password: 'morethan6chars'
}

const invalidData = {
  email: 123,
  password: 'a'
}

describe('Login Validation', () => {
  test('should call next function when validation succeeds', async () => {
    const { req, res, next} = utils.setup()
    req.body = validData

    await loginValidator(req, res, next)

    expect(next).toHaveBeenCalled()
  })

  test('should NOT call next function when validation succeeds', async () => {
    const { req, res, next} = utils.setup()
    req.body = invalidData

    await loginValidator(req, res, next)

    expect(next).not.toHaveBeenCalled()
    expect(res.status).toBe(422)
    expect((res.json as any).message).toBe('Validation failed.')
  })
})