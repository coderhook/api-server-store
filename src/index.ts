import config from '../src/config/index'

import app from "./app"
import { connectDB } from "./db/mongoose"


app.listen(config.port, () => {
    connectDB()
    console.log('Connected to mongoDB to URI: ', config.database)
    console.log(`Project running on ${config.url}`)
})