import utils from "../../../tests-utils/utils"
import userController from "../user.controller"
import {initDataBase, dbClose} from '../../../tests-utils/fixtures/db'

let userTestRegistered

beforeAll( async () => {
  userTestRegistered = initDataBase()
})

afterAll( async () =>  {
  dbClose()
})


describe('User controller', () => {
  describe('userProfile', () => {
    test('should get profile from the authenticated user', async () => {
      const { req, res } = utils.setup()
      req.body = {authUser: userTestRegistered}

      await userController.userProfile(req, res)

      expect(res.status).toBe(200)
      expect(res.json.data.user.email).toBe(userTestRegistered.email)  

    })
  })
})