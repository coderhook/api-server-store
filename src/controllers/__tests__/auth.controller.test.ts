import authController, {testRoute } from "../auth.controller";
import utils from '../../../tests-utils/utils'
import mongoose from 'mongoose'
import {initDataBase, userTest} from '../../../tests-utils/fixtures/db'

beforeAll( async () => {
  initDataBase()
})

afterAll( async () =>  {
  await mongoose.connection.close()
})

describe('Login controller', () => {
  test('should respond with 200 to the test auth route', async () => {
    const { req, res} = utils.setup()
    const response: any = await testRoute(req, res)
    expect(res.status).toBe(200)
    expect(response.json.message).toBe('Route auth is working as expected.')
  })

  test('should return 401 when user does not exist', async () => {
    const { req, res } = utils.setup()
    req.body = {
      email: 'NonExistentEmail',
      password: 'fakePassword'
    }
    const result: any = await authController.login(req, res)
    expect(result.status).toBe(401)
  })

  test('should return 401 when email exists but password is incorrect', async () => {
    const { req, res } = utils.setup()
    req.body = {
      email: userTest.email,
      password: 'fakePassword'
    }
    const test: any = await authController.login(req, res)
    expect(test.status).toBe(401)
  })

  test('should return status 200 when email exists in the db and password is correct', async () => {
    const { req, res } = utils.setup()
    req.body = {
      email: userTest.email,
      password: userTest.password
    }
    await authController.login(req, res)

    expect(res.status).toBe(200)
    expect((res.send as any).data.user.email).toBe(userTest.email)
  })
})

describe('Register Controller',  () => {
  test('should register a new user', async () => {
    const newUser = {name: 'newUser', email: 'newUser@email.com', password: 'newPassword'}
    const { req, res} = utils.setup()

    req.body = {...newUser}

    await authController.register(req, res)

    expect(res.status).toBe(201)
    expect((res.json as any).data.user.email).toBe(newUser.email)
    expect((res.json as any).message).toEqual('Account Registered.')
  })

  test('should return 422 `Unprocessable Entity` when user cannot be registered', async() => {
    const { req, res} = utils.setup()

    req.body = {...userTest}

    await authController.register(req, res)

    expect(res.status).toBe(422)
    expect((res.json as any).message).toBe('Registration failed!')

  })
})


