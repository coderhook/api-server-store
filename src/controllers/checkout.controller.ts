var stripe = require('stripe')('sk_test_51H2kkgBnU8pVZGbDlwAKaJap6wCwLhpL3arpEN4OjnVONZF2SKjM4YoPuOaeyuY4OgDdE3oOxNc8BYSXFO7p7Icx00NVpsN4x8')



const createStripeSession = async (req, res) => {
  const {cart, data, customerId} = req.body

  console.log('REQ ----> ', {customerId})

  const lineItems = cart.map(c => ({amount: c.price, currency: 'eur', name: 'Model: ' + c.model + ' Size: ' + c.size, quantity: c.quantity}))

  try{
    const session = await stripe.checkout.sessions.create({
      payment_method_types: ['card'],
      line_items: lineItems,
      mode: 'payment',
      success_url: 'http://localhost:3000/success',
      cancel_url: 'http://localhost:3000/cancel',
      customer: customerId,
      // customer_email: data.customer.email,
    })
  
    return res.status(201).json({
      session_id: session.id
    })
  } catch(e){
    console.log(e)
  }


}


interface CustomerObject {
  address: {
    city: string
    country: string
    line1: string
    line2?: string
    postal_code: string
    state: string
  }
  description?: string
  email: string
  name: string
  phone: string
  shipping: {
    address: {
      city: string
      country: string
      line1: string
      line2?: string
      postal_code: string
      state: string
    }
    name: string
    phone?: string
  }

}

const createStripeCustomer = async (req, res) => {

  const {userData} = req.body

  console.log({userData})

  const {customer, shipping_address, billing_address} = userData

  const customerObject: CustomerObject = {
    address: {
      city: billing_address.city,
      country: billing_address.country,
      line1: billing_address.line_1,
      line2: billing_address.line_2 || null,
      postal_code: billing_address.county,
      state: billing_address.city
    },
    email: customer.email,
    name: billing_address.first_name + ' ' + billing_address.last_name,
    phone: customer.phone || null,
    shipping: {
      address: {
        city: shipping_address.city,
        country: shipping_address.country,
        line1: shipping_address.line_1,
        line2:shipping_address.line_2 || null,
        postal_code: shipping_address.county,
        state: shipping_address.city
      },
      name: shipping_address.first_name + ' ' + shipping_address.last_name
    }
  }

  try {
    const {id} = await stripe.customers.create(customerObject)

    return res.status(201).json({id})
  } catch (e) {
    console.log({e})
  }


}

export default {
  createStripeSession,
  createStripeCustomer
}