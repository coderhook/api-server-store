import { Request, Response } from "express"
import Product from "../models/Product"
import Stripe from "stripe"
const stripe = require('stripe')('sk_test_51H2kkgBnU8pVZGbDlwAKaJap6wCwLhpL3arpEN4OjnVONZF2SKjM4YoPuOaeyuY4OgDdE3oOxNc8BYSXFO7p7Icx00NVpsN4x8')

interface Product {
  _id: string
  model: string
  name: string
  description: string
  category: string
  images: string[]
  price: {
    unit_amount: number
    currency: string
  }
  inventory: {
    "p35": number
    "p36": number
    "p37": number
  }
  background_colour: string | null

}

interface StripeProduct {
  id: string
  name: string
  description: string
  images: string[]
  type: string
  metadata: {
    category: string
    model: string
    p35: number
    p36: number
    p37: number
  }
}

const addProduct = async (req, res) => {

  const data = req.body
  // console.log('HERE', {product})
  // const productExists = await Product.findOne({id: product.id})

  // if(!!productExists) {
  //   return res.status(422).json({message: 'Product registration failed!'})
  // }

  // const createdProduct = await Product.create(product)
  try {
    const createdProduct: Product = await Product.create(data)
    const { model, images, price, name, description, category, inventory, background_colour} = createdProduct
    const stripeProduct: StripeProduct = {
      id: String(createdProduct._id),
      images,
      name,
      description, 
      type: 'good',
      metadata: {
        category,
        model,
        ...inventory
      }
    }

    const stripePrice = {
      product: stripeProduct.id,
      ...price
    }

    const createdStripeProduct = await stripe.products.create(stripeProduct)
    
    if(!createdStripeProduct) res.status(400).res('error creating the product')
    if(createdStripeProduct) await stripe.prices.create(stripePrice)

    

    return res.status(201).json({
      data: {createdProduct},
      message: 'Product Registered.'
    })
  } catch(e) {
    console.error
  }

}

const getProducts = async (req, res) => {
  const listProducts = await Product.find()

  const stripeProds = await stripe.products.list()

  return res.status(200).json({
    data: listProducts,
    stripe: stripeProds.data,
    message: `${listProducts.length} Products retrieved`
  })
}

export default {
  addProduct,
  getProducts
}