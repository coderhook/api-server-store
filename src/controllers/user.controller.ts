import { Request, Response } from "express"
import User from "../models/User"
import multer from 'multer'

const userProfile = (req, res) => {
  const user = req.body.authUser
  return res.status(200).json({
    data: {user},
    message: `Profile for user ${user}`
  })
}


const uploadAvatar = (req, res) => {


  res.status(200).send()
}

export default {
  userProfile,
  uploadAvatar
}