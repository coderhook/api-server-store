import { Request, Response } from "express"
import User from "../models/User"

export const testRoute = (req: Request, res: Response) => {
  return res.status(200).json({message: 'Route auth is working as expected.'})
}

const login = async (req: any, res: Response) => {
	const { name, email, password} = req.body

  const failureResponse = () =>
    res.status(401).json({
      message: 'Authentication failed.'
  })

  const user = await User.findOne({email})
  if(!user) {
    return failureResponse()

    // temporarily I will auto registerwhenuser is not in db
    // await User.create({name, email, password})
  }

  const passwordIsCorrect = user.comparePasswords(password)

  if(!passwordIsCorrect) {
    return failureResponse()
  }

  const token = user.generateToken()

  return res.status(200).send({
    data: {
			user,
			token
    },
    message: 'Login Succesful.'
  
  })
}

const register = async (req, res) => {
  const { name, email, password} = req.body

  const userExists = await User.findOne({email})

  if(!!userExists) {
    return res.status(422).json({message: 'Registration failed!'})
  }

  const user = await User.create({name, email, password})

  const token = user.generateToken()

  return res.status(201).json({
    data: {user, token},
    message: 'Account Registered.'
  })
}

export default {
  login,
  register
}
