// import mongoose from 'mongoose'
// import User from '../models/User'
const mongoose = require('mongoose')
const validator = require('validator')

export const connectDB = () => mongoose.connect(process.env.DB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
})

// Create the User model --> created in models/User.ts
// const User = mongoose.model('User', {
//   name: {
//     type: String,
//     required: true,
//     trim: true
//   },
//   email: {
//     type: String,
//     required: true,
//     trim: true,
//     lowercase: true,
//     validate(value){
//       if(!validator.isEmail(value)){
//         throw new Error('Email is invalid')
//       }
//     }
//   },
//   password: {
//     type: String,
//     required: true,
//     trim: true
//   }
// })

// const me = new User({
//   name: '     Pedro   ',
//   email: 'pedRRRRo@email.com',
//   password: 'passwordNotHashes'
// })

// const saveUser = async (me) => {
//   const user = await me.save()
//   return user
// }

// saveUser(me).then(() => {
//     console.log(me)
//   }).catch((e) => console.log(e))

// mongoose.connection.close()