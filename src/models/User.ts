import config from '../config'
import Bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'
import mongoose from 'mongoose'
import randomstring from 'randomstring'

export const UserSchema = new mongoose.Schema({
  name: String,
  email: String,
  createdAt: Date,
  updatedAt: Date,
  password: String,
  emailConfirmedAt: Date,
  emailConfirmCode: String
})


// Hash the plain text password
UserSchema.pre('save', async function() {
  this.password = Bcrypt.hashSync(this.password, 8)
  this.emailConfirmCode = randomstring.generate()
})

UserSchema.methods.comparePasswords = function(password) {
  return Bcrypt.compareSync(password, this.password)
}

UserSchema.methods.generateToken = function() {
  return jwt.sign({ id: this._id }, config.jwtSecret, {expiresIn: '2 hours'})
}

UserSchema.methods.toJSON = function(){
  const user = this
  const userObject = user.toObject()

  delete userObject.password

  return userObject
}

export default mongoose.model('User', UserSchema)