
import mongoose from 'mongoose'

export const ProductSchema = new mongoose.Schema({
  model: String,
  background_colour: String || null,
  name: String,
  description: String,
  images: [String],
  category: String,
  price: {
    unit_amount: Number,
    currency: String
  },
  inventory: {},
})

ProductSchema.methods.toJSON = function(){
  const product = this
  const productObject = product.toObject()

  return productObject
}

export default mongoose.model('Product', ProductSchema)



// 
// {
//   "product": {
//     "id": "4",
//     "name":"Red Sandal",
//     "description":"Red Sandal",
//     "metadata": {
//       "35": "2",
//       "36": 0,
//       "37": 4
//       },
//     "images": ["https://vegan-store-images.s3-eu-west-1.amazonaws.com/verano20/sandalias/2025+TRENZA+ROJA+P-CU%C3%91A+35-41.JPG"],
//     "type": "good"
//   },
//   "price": {
//       "product": "4",
//       "unit_amount": "4990",
//       "currency": "eur"
//     }

// }
// 