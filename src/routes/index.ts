import express from 'express'
import authRouter from './v1/auth'
import userRouter from './v1/user'
import productRouter from './v1/product'
import checkoutRouter from './v1/checkout'


const v1Router: express.Application = express()

v1Router.use('/auth', authRouter)
v1Router.use('/users', userRouter)
v1Router.use('/products', productRouter)
v1Router.use('/checkout', checkoutRouter)

export default v1Router