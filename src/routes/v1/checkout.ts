import express from 'express'
import auth from '../../middleware/auth'
import checkoutController from '../../controllers/checkout.controller'

const checkoutRouter = express()

checkoutRouter.get('/', (req, res) => res.json("checkout get"))
checkoutRouter.post('/', checkoutController.createStripeSession)

checkoutRouter.post('/create-customer', checkoutController.createStripeCustomer)

export default checkoutRouter