import express from 'express'
import auth from '../../middleware/auth'
import userController from '../../controllers/user.controller'
import multer from 'multer'

const userRouter = express()

userRouter.get('/profile', auth, userController.userProfile)

const upload = multer({
  dest: 'images/avatar'
})

upload.single('upload')

userRouter.post('/profile/avatar', upload.single('upload'), userController.uploadAvatar)

export default userRouter

