import express from 'express'
import authController, { testRoute } from '../../controllers/auth.controller'
import loginValidation from '../../validations/login'
import auth from '../../middleware/auth'

const authRouter = express()

authRouter.get('/test', auth, testRoute)

authRouter.post('/login', loginValidation, authController.login)

// to make it work we have to store the token in some short of db, or in the db or in memory db like redis for instance.
authRouter.post('/logout', (req, res) => {
  // check for the token in a db on is stored
  // remove it from the db
})

authRouter.post('/register', authController.register)

export default authRouter