import supertest from 'supertest'
import User from '../../../models/User'
import mongoose from 'mongoose'
import server from '../../../app'

const app = supertest(server)

const userTest = {
  name: 'test',
  email: 'test@email.com',
  password: 'password'
} 

const newUser = {name: 'newUser', email: 'newUser@email.com', password: 'newPassword'}

let loggedUser

beforeAll( async () => {
  await mongoose.connect('mongodb://localhost:27018/server-store-test', {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })

  await User.deleteMany()
  loggedUser = await User.create(userTest) 
})

afterAll( async () =>  {
  await mongoose.connection.close()
})

describe('User Routes', () => {
  test('should get the profile when user is authenticated', async () => {
    const token = loggedUser.generateToken()

    await app.get('/api/v1/users/profile')
                              .set({'Authorization': `Bearer ${token}`})
                              .expect(200)
    
  })

  test('should not get profile for unauthenticated user', async () => {
    await app.get('/api/v1/users/profile')
                              .send()
                              .expect(401)})
})