import supertest from 'supertest'
import User from '../../../models/User'
import server from '../../../app'
import {initDataBase, userTest, newUser, dbClose} from '../../../../tests-utils/fixtures/db'

const app = supertest(server)

// const newUser = {name: 'newUser', email: 'newUser@email.com', password: 'newPassword'}

let userTestRegistered

beforeAll( async () => {
  userTestRegistered = initDataBase()
})

afterAll( async () =>  {
  dbClose()
})

describe('Auth Routes', () => {
  describe('Login routes', () => {
    test('should be able to login a user', async () => {
      const response = await app.post('/api/v1/auth/login').send(userTest).expect(200)

      expect(response.body.data.user.email).toBe(userTest.email)
      expect(response.body.message).toBe('Login Succesful.')
    })

    test('should send Authenthication failed when data does not exist on database', async () => {
      const response = await app.post('/api/v1/auth/login').send(newUser).expect(401)
      expect(response.body.message).toBe('Authentication failed.')
    })
  })

  describe('Register routes', () => {
    test('should be able to register a new user', async () => {
      const response = await app.post('/api/v1/auth/register').send(newUser).expect(201)

      // Get user registered from db
      const user = await User.findOne({email: newUser.email})

      expect(response.body.message).toBe('Account Registered.')
      expect(user).not.toBeNull()
      expect(user.password).not.toBe(newUser.password)
    })

    test('should not register user if email already exists in the database', async () => {
      const response = await app.post('/api/v1/auth/register').send(userTest).expect(422)
    })
  })
})