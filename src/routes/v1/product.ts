import express from 'express'
import auth from '../../middleware/auth'
import productsController from '../../controllers/products.controller'

const productRouter = express()

productRouter.get('/', productsController.getProducts)
productRouter.post('/', productsController.addProduct)

export default productRouter

