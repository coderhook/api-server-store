import dotenv from 'dotenv'

dotenv.config()

export default {
    url: process.env.APP_URL || 'http://localhost:3001',
    port: process.env.PORT || 3001,
    environment: process.env.NODE_ENV || 'development',

    jwtSecret: process.env.JWT_SECRET || '1234',
    development: process.env.NODE_ENV === 'development',
    production: process.env.NODE_ENV === 'production',

    database: process.env.DB_URI || "mongodb://localhost:27017/test-project_api", // TODO: tochange  the name of main db
    database_test: process.env.DB_TEST_URI
}