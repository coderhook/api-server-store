import config from '../config'
import jwt from 'jsonwebtoken'
import User from '../models/User'
import { Response, NextFunction, Request } from 'express'

export default async (req, res: Response, next: NextFunction) => {
  try {
      const token =
          req.body.token ||
          req.headers['authorization'].split(' ')[1] ||
          req.query.token

      const payload = jwt.verify(token, config.jwtSecret)
      const authUser = await User.findById( (payload as Record<string, any>).id)

      req.body.authUser = authUser

      return next()
  } catch (error) {
      return res.status(401).json({
          message: 'Unauthenticated.'
      })
  }
}