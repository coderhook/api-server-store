import auth from '../middleware/auth'
import { Response } from 'express'
import utils from '../../tests-utils/utils'
import {initDataBase, dbClose} from '../../tests-utils/fixtures/db'

let result

beforeAll( async () => {
  result = await initDataBase()
})

afterAll( async () =>  {
  dbClose()
})

describe('auth middleware', () => {
  test('should call next funtion when user has a valid token', async () => {

    const token = result.userTestRegistered.generateToken()

    const req = {
      body: {
        token
      }
    }
    const res = {
      status: jest.fn(),
      json: jest.fn()
    } as unknown as Response
    
    const next = jest.fn()

    await auth(req, res, next)
    expect(next).toHaveBeenCalled()
  })

  test('should respond with status 401 when user can not be authenticated', async () => {
    const {req, res, next} = utils.setup()
    req.body = {access_token: 'invalid token'}

    const nonAuth: any = await auth(req, res as any, next)

    expect(next).not.toHaveBeenCalled()
    expect(res.status).toEqual(401)
    expect(nonAuth.status).toBe(401)
    expect(nonAuth.json.message).toBe('Unauthenticated.')
  })
})