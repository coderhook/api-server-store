import express, { Response, Request, application } from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import morgan from 'morgan'
import v1Router from './routes'

const app = express();
app.use(cors())
app.use(bodyParser.json());
app.use(morgan('combined'))
app.use('/api/v1', v1Router)

app.get('/', (req: Request, res: Response) => {
  res.send("Hello World ")
})

export default app